class Exercise

  # Assume that "str" is a sequence of words separated by spaces.
  # Return a string in which every word in "str" that exceeds 4 characters is replaced with "marklar".
  # If the word being replaced has a capital first letter, it should instead be replaced with "Marklar".
  def self.marklar(str)

    #Substitute any word starting with a lower case letter, followed by 4 or more characters with 'marklar'
    str = str.gsub(/\b[a-z]{5,}\b/, 'marklar')

    #Substitute any word starting with a capital letter, followed by 4 or more characters with 'Marklar'
    str = str.gsub(/\b[A-Z][a-z]{4,}\b/, 'Marklar')

    # Return the result
    return str
  end

  # Return the sum of all even numbers in the Fibonacci sequence, up to
  # the "nth" term in the sequence
  # eg. the Fibonacci sequence up to 6 terms is (1, 1, 2, 3, 5, 8),
  # and the sum of its even numbers is (2 + 8) = 10
  def self.even_fibonacci(nth)
    # Loop up to the nth term of the fibnoacci sequence
    current_term = 0
    previous_term = 0
    term = 0
    total = 0
    for i in 1..nth

      # Add the previous 2 terms together to get the next fibonacci term...
      term = current_term + previous_term

      # Check if it was the first term
      if term == 0
        term = 1
      end
      
       # Reassign current and previous terms
      previous_term = current_term
      current_term = term

      # Check if the fibonacci term is even
      if term % 2 == 0

        # If the term is even, then add it to our total
        total += term
      end
    end

    # Return total
    return total
  end

end
